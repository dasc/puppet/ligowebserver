# proxy module
#
# @summary Loads and enables the proxy, proxy_http, and proxy_http2 modules
#
# @example Basic usage
#    include ligowebserver::modules::proxy
#
class ligowebserver::modules::proxy (
) {
    class {'apache::mod::proxy': }
    class {'apache::mod::proxy_http': }
    class {'apache::mod::proxy_http2': }
    class {'apache::mod::proxy_wstunnel': }
}
