# Datafind service configuration for apache
#
# @summary Configures the datafind service for ssl and non-ssl connections
#
# @example Basic usage
#    include ligowebserver::content::datafind
#
class ligowebserver::content::datafind (
    Boolean $use_shib = false,
) {
    if $use_shib {
        include ligowebserver::shib
    } else {
        include ligowebserver::ssl
    }
    include ligowebserver::nossl

    $server_name = lookup('apache::server_name', undef, undef, $facts['networking']['fqdn'])

    include ligowebserver::modules::proxy

    $datafind_custom_fragment = @(EOF/L)
    ####################
    # apache::vhost::fragment for 'datafind'
    ProxyRequests Off
    ProxyPass "/" "http://127.0.0.1:8080/" retry=0 ttl=60
    ProxyPassReverse "/" "http://127.0.0.1:8080/"
| EOF

    apache::vhost::fragment {'datafind-proxypass-ssl':
        vhost    => "${server_name}-ssl",
        priority => 10,
        content  => $datafind_custom_fragment,
    }

    apache::vhost::fragment {'datafind-proxypass-http':
        vhost    => "${server_name}-http",
        priority => 10,
        content  => $datafind_custom_fragment,
    }

    apache::vhost::fragment {'datafind-ssl':
        vhost    => "${server_name}-ssl",
        priority => 10,
        content  => epp('ligowebserver/etc/httpd/conf.d/ssldir.epp', {
            description     => 'datafind (ssl)',
            location        => '/',
            reqs            => [{
                type         => 'all',
                requirements => [
                    'all granted',
                ],
            }],
            custom_fragment => [
                'SSLVerifyClient optional',
                'SSLVerifyDepth 10',
                'SSLOptions +ExportCertData +StrictRequire +LegacyDNStringFormat',
                'SSLOptions +StdEnvVars',
                'RequestHeader set SSL_CLIENT_S_DN "%{SSL_CLIENT_S_DN}s"',
                'RequestHeader set SSL_CLIENT_I_DN "%{SSL_CLIENT_I_DN}s"',
            ],
            use_shib        => $use_shib,
        }),
    }

    apache::vhost::fragment {'datafind-http':
        vhost    => "${server_name}-http",
        priority => 10,
        content  => epp('ligowebserver/etc/httpd/conf.d/ssldir.epp', {
            description => 'datafind (http)',
            location    => '/',
            reqs        => [{
                type         => 'all',
                requirements => [
                    'all granted',
                ],
            }],
            use_shib    => false,
            use_ssl     => false,
        }),
    }
}
