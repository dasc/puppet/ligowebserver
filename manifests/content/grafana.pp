# Reverse proxy to grafana
#
# @summary Configures a reverse proxy to access to the grafana application
#
# @example Basic usage
#    include ligowebserver::content::grafana
#
class ligowebserver::content::grafana (
    Boolean $use_shib = true,
) {
    if $use_shib {
        include ligowebserver::shib
    } else {
        include ligowebserver::ssl
    }

    $server_name = lookup('apache::server_name', undef, undef, $facts['networking']['fqdn'])

    include ligowebserver::modules::proxy

    $grafana_custom_fragment = @(EOF/L)
    ####################
    # apache::vhost::fragment for 'grafana'
    ProxyRequests Off
    ProxyPass /Shibboleth.sso !
    ProxyPass /server-status !
    ProxyPass / http://localhost:3000/ disablereuse=On

    RequestHeader set REMOTE_USER %{REMOTE_USER}s
    RequestHeader edit REMOTE_USER @LIGO.ORG ''

    <Proxy *>
        AuthName "This content is viewable by only LIGO/Virgo personnel. Please enter your LIGO Directory name, e.g. albert.einstein, and password to continue."
        AuthType shibboleth
        ShibRequestSetting requireSession 1
        <RequireAll>
            require shib-session
            require shib-attr isMemberOf Communities:LSCVirgoLIGOGroupMembers
        </RequireAll>
    </Proxy>
| EOF

    apache::vhost::fragment {'grafana-proxypass-ssl':
        vhost    => "${server_name}-ssl",
        priority => 10,
        content  => $grafana_custom_fragment,
    }
}
