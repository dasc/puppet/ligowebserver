# Vulture
#
# @summary Configures access to the vulture monitoring pages
#
# @example Basic usage
#    include ligowebserver::content::vulture
#
class ligowebserver::content::vulture (
    Boolean $use_shib = true,
) {
    if $use_shib {
        include ligowebserver::shib
    } else {
        include ligowebserver::ssl
    }

    $server_name = lookup('apache::server_name', undef, undef, $facts['networking']['fqdn'])

    apache::vhost::fragment {'vulture':
        vhost    => "${server_name}-ssl",
        priority => 10,
        content  => epp('ligowebserver/etc/httpd/conf.d/ssldir.epp', {
            description => 'Vulture',
            location    => '/vulture',
            directory   => '/opt/vulture/web',
            reqs        => [{
                type         => 'all',
                requirements => [
                    'shib-session',
                    'shib-attr isMemberOf Communities:LSCVirgoLIGOGroupMembers',
                ],
            }],
            use_shib    => $use_shib,
        }),
    }
}
