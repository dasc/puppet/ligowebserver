# SSL vhost with shibboleth authentication enabled
#
# @summary Sets up an apache vhost with shibboleth authentication.
#
# @note This is normally loaded automatically by a content-specific profile.
#
# @see ligowebserver::ssl.pp
#
# @example Basic usage
#    include ligowebserver::shib
#
# @example Add permissions for a directory
#    include ligowebserver::shib
#    apache::vhost::fragment {'mydir-ssl':
#        vhost    => "default-ssl",
#        priority => 10,
#        content  => epp('ligowebserver/etc/httpd/conf.d/ssldir.epp', {
#            description => 'mydir (shib)',
#            directory   => '/var/www/html/mydir',
#            reqs        => [{
#                type         => 'all',
#                requirements => [
#                    'shib-session',
#                    'shib-attr isMemberOf Communities:LSCVirgoLIGOGroupMembers',
#                ],
#            }],
#        }),
#    }
#
# @example Add permissions for a directory using hiera
#    include ligowebserver::shib
#
#    ligowebserver::apache::shibdirs:
#      'mydir':
#        'description': 'mydir'
#        'directory': '/var/www/html/mydir'
#        'reqs': 
#          - type: 'all'
#            requirements:
#            - 'shib-session'
#            - 'shib-attr isMemberOf Communities:LSCVirgoLIGOGroupMembers'
class ligowebserver::shib (
) {
    include ligowebserver::ssl
    $server_name = lookup('apache::server_name', undef, undef, $facts['networking']['fqdn'])

    include ligowebserver::common

    $apache_shib_custom_fragment = @(EOF/L)

####################
# apache::vhost::fragment for 'shibboleth'
#
# Turn this on to support "require valid-user" rules from other
# mod_authn_* modules, and use "require shib-session" for anonymous
# session-based authorization in mod_shib.
#
ShibCompatValidUser Off

#
# Ensures handler will be accessible.
#
<Location /Shibboleth.sso>
  AuthType None
  Require all granted
</Location>

#
# Used for example style sheet in error templates.
#
<IfModule mod_alias.c>
  <Location /shibboleth-sp>
    AuthType None
    Require all granted
  </Location>
  Alias /shibboleth-sp/main.css /usr/share/shibboleth/main.css
</IfModule>
#
# Configure the module for content.
#
# You MUST enable AuthType shibboleth for the module to process
# any requests, and there MUST be a require command as well. To
# enable Shibboleth but not specify any session/access requirements
# use "require shibboleth".
#
#<Location /secure>
#  AuthType shibboleth
#  ShibRequestSetting requireSession 1
#  require shib-session
#</Location>

| EOF

    class {'apache::mod::shib':
        mod_full_path => '/usr/lib64/shibboleth/mod_shib_24.so',
    }

    apache::vhost::fragment {'00shibboleth':
        vhost    => "${server_name}-ssl",
        priority => 10,
        content  => $apache_shib_custom_fragment,
    }

    # Add any shibboleth protected directories defined in hiera
    lookup('ligowebserver::apache::shibdirs', undef, undef, {}).each | String $name, Ligowebserver::HttpDirent $dirent | {
        apache::vhost::fragment {$name:
            vhost    => "${server_name}-ssl",
            priority => 10,
            content  => epp('ligowebserver/etc/httpd/conf.d/ssldir.epp', {
                description     => $dirent['description'],
                location        => $dirent['location'],
                directory       => $dirent['directory'],
                directoryindex  => $dirent['directoryindex'],
                reqs            => $dirent['reqs'],
                custom_fragment => $dirent['custom_fragment'],
                use_shib        => true,
            }),
        }
    }
}
