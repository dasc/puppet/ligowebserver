# Jupyter Hub
#
# @summary Configures the jupyterhub application
#
# @example Basic usage
#    include ligowebserver::content::jupyter
#
class ligowebserver::content::jupyter (
    Boolean $use_shib = true,
) {
    if $use_shib {
        include ligowebserver::shib
    } else {
        include ligowebserver::ssl
    }
    include ligowebserver::modules::proxy

    $server_name = lookup('apache::server_name', undef, undef, $facts['networking']['fqdn'])

    $jupyter_custom_fragment = @(EOF/L)
    ####################
    # apache::vhost::fragment for 'jupyterhub'
    ProxyVia On
    ProxyRequests Off
    ProxyPreserveHost on
    ServerAlias jupyter*

    Header edit Origin HOSTNAME.TLD 127.0.0.1:8000
    RequestHeader edit Origin HOSTNAME.TLD 127.0.0.1:8000
    Header edit Referrer HOSTNAME.TLD 127.0.0.1:8000
    RequestHeader edit Referrer HOSTNAME.TLD 127.0.0.1:8000

    RequestHeader set REMOTE_USER %{REMOTE_USER}s
    RequestHeader edit REMOTE_USER @LIGO.ORG ''
    RequestHeader edit REMOTE_USER @ligo.org ''


     <Location "/">
        AuthType shibboleth
        ShibRequestSetting requireSession 1
        <RequireAll>
            <RequireAll>
                Require method GET POST PUT CONNECT OPTIONS PATCH DELETE
                Require shib-session
            </RequireAll>
            <RequireAny>
                Require shib-attr isMemberOf Communities:LVC:LVCGroupMembers
                Require shib-attr isMemberOf gw-astronomy:KAGRA-LIGO:ask.ligo.org
                Require shib-attr isMemberOf gw-astronomy:KAGRA-LIGO:DASWG:wiki
            </RequireAny>
        </RequireAll>

        ProxyPass http://127.0.0.1:8000/
        ProxyPassReverse http://127.0.0.1:8000/
    </Location>

   <Location "/login">
       Options +Indexes
       ProxyPass "!"
       Satisfy Any
       Require all granted
       DirectoryIndex /etc/shibboleth-ds/index.html
   </Location>

    <LocationMatch "/(user/[^/]+)/(api/kernels/[^/]+/channels|terminals/websocket)/?">

        ProxyPass ws://127.0.0.1:8000
        ProxyPassReverse ws://127.0.0.1:8000
    </LocationMatch>
| EOF

    apache::vhost::fragment {'jupyter-ssl':
        vhost    => "${server_name}-ssl",
        priority => 10,
        content  => $jupyter_custom_fragment,
    }
}
