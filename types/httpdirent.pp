type Ligowebserver::HttpDirent = Struct[{
    description     => String,
    location        => Optional[String],
    directory       => Optional[String],
    directoryindex  => Optional[String],
    custom_fragment => Optional[Variant[String, Array[String]]],
    reqs            => Array[Ligowebserver::HttpRequire],
    use_shib        => Optional[Boolean],
    use_ssl         => Optional[Boolean],
}]
