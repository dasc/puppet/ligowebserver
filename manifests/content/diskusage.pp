# duc cgi apache config
#
# @summary Configures the duc cgi for apache
#
# @example Basic usage
#    include ligowebserver::content::diskusage
#
class ligowebserver::content::diskusage (
    Boolean $use_shib = true,
    String $location = '/diskusage',
    String $directory = '/var/www/html/diskusage',
) {
    if $use_shib {
        include ligowebserver::shib
    } else {
        include ligowebserver::ssl
    }

    include ligowebserver::content::cgi

    $server_name = lookup('apache::server_name', undef, undef, $facts['networking']['fqdn'])

    apache::vhost::fragment {'duc':
        vhost    => "${server_name}-ssl",
        priority => 10,
        content  => epp('ligowebserver/etc/httpd/conf.d/ssldir.epp', {
            description     => 'diskusage (duc)',
            location        => $location,
            directory       => $directory,
            reqs            => [{
                type         => 'all',
                requirements => [
                    'shib-session',
                    'shib-attr isMemberOf Communities:LSCVirgoLIGOGroupMembers',
                ],
            }],
            use_shib        => $use_shib,
            custom_fragment => ['Options ExecCGI SymlinksIfOwnerMatch FollowSymlinks'],
        }),
    }
}
