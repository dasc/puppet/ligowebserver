# Ganglia web page settings
#
# @summary Configures access to the ganglia web pages
#
# @example Basic usage
#    include ligowebserver::content::ganglia
#
class ligowebserver::content::ganglia (
    Boolean $use_shib = true,
) {
    if $use_shib {
        include ligowebserver::shib
    } else {
        include ligowebserver::ssl
    }

    include ligowebserver::content::php

    $server_name = lookup('apache::server_name', undef, undef, $facts['networking']['fqdn'])

    apache::vhost::fragment {'ganglia':
        vhost    => "${server_name}-ssl",
        priority => 10,
        content  => epp('ligowebserver/etc/httpd/conf.d/ssldir.epp', {
            description => 'ganglia',
            location    => '/ganglia',
            directory   => '/usr/share/ganglia',
            reqs        => [{
                type         => 'all',
                requirements => [
                    'shib-session',
                    'shib-attr isMemberOf Communities:LSCVirgoLIGOGroupMembers',
                ],
            }],
            use_shib    => $use_shib,
        }),
    }
}
