# Userdir
#
# @summary Configures access to the user public_html directory
#
# This profile only enables the use of the userdir module.  It does not
# configure any special permissions for the user's public_html directory.
# That should be done either in hiera or by writing a custom profile.
#
# @example Basic usage
#    include ligowebserver::content::userdir
#
class ligowebserver::content::userdir (
    String $userdir = 'public_html',
) {
    include ligowebserver::ssl
    include ligowebserver::modules::userdir

    $server_name = lookup('apache::server_name', undef, undef, $facts['networking']['fqdn'])

    apache::vhost::fragment {'userdir-default':
        vhost    => "${server_name}-ssl",
        priority => 10,
        content  => "    Userdir ${userdir}\n",
    }
}
