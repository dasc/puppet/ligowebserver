# Nagios
#
# @summary Configures access to the nagios pages and cgi
#
# @example Basic usage
#    include ligowebserver::content::nagios
#
class ligowebserver::content::nagios (
    Boolean $use_shib = true,
) {
    if $use_shib {
        include ligowebserver::shib
    } else {
        include ligowebserver::ssl
    }

    $server_name = lookup('apache::server_name', undef, undef, $facts['networking']['fqdn'])

    apache::vhost::fragment {'nagios-cgi':
        vhost    => "${server_name}-ssl",
        priority => 10,
        content  => epp('ligowebserver/etc/httpd/conf.d/ssldir.epp', {
            description     => 'Nagios CGI',
            scriptalias     => '/nagios/cgi-bin/',
            directory       => '/usr/lib64/nagios/cgi-bin/',
            reqs            => [{
                type         => 'all',
                requirements => [
                    'shib-session',
                    'shib-attr isMemberOf Communities:LSCVirgoLIGOGroupMembers',
                ],
            }],
            custom_fragment => ['Options ExecCGI', 'AllowOverride None'],
            use_shib        => $use_shib,
        }),
    }

    apache::vhost::fragment {'nagios-static':
        vhost    => "${server_name}-ssl",
        priority => 10,
        content  => epp('ligowebserver/etc/httpd/conf.d/ssldir.epp', {
            description     => 'Nagios static content',
            location        => '/nagios',
            directory       => '/usr/share/nagios/html',
            reqs            => [{
                type         => 'all',
                requirements => [
                    'shib-session',
                    'shib-attr isMemberOf Communities:LSCVirgoLIGOGroupMembers',
                ],
            }],
            custom_fragment => ['Options None', 'AllowOverride None'],
            use_shib        => $use_shib,
        }),
    }
}
