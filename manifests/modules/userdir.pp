# userdir module
#
# @summary Loads and enables the userdir module
#
# @example Basic usage
#    include ligowebserver::modules::userdir
#
class ligowebserver::modules::userdir (
) {
    class {'apache::mod::userdir': }
}
