# Webserver status url
#
# @summary Configures a url for getting webserver status and metrics
#
# @example Basic usage
#    include ligowebserver::content::status
#
class ligowebserver::content::status (
) {
    include ligowebserver::ssl

    $server_name = lookup('apache::server_name', undef, undef, $facts['networking']['fqdn'])

    class {'apache::mod::status': }

    $status_custom_fragment = @(EOF/L)

    ####################
    # apache::vhost::fragment for 'status'

    <Location /server-status>
        <IfModule mod_proxy.c>
            ProxyPass "!"
        </IfModule>
        SetHandler server-status
        Require local
    </Location>
| EOF

    apache::vhost::fragment {'status':
        vhost    => "${server_name}-ssl",
        priority => 10,
        content  => $status_custom_fragment,
    }
}
