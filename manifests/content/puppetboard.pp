# Puppetboard
#
# @summary Configures access to the puppetboard web service
#
# @example Basic usage
#    include ligowebserver::content::puppetboard
#
class ligowebserver::content::puppetboard (
    Boolean $use_shib = true,
    Optional[String] $content_dir = '/srv/puppetboard',
    Optional[String] $wsgi_dir = '/srv/puppetboard/wsgi.py',
) {
    if $use_shib {
        include ligowebserver::shib
    } else {
        include ligowebserver::ssl
    }

    $server_name = lookup('apache::server_name', undef, undef, $facts['networking']['fqdn'])

    include ligowebserver::modules::wsgi

    $puppetboard_custom_fragment = @("EOF"/L)
    ####################
    # apache::vhost::fragment for 'puppetboard'
    WSGIDaemonProcess puppetboard user=apache group=apache threads=5
    WSGIScriptAlias /puppetboard ${wsgi_dir}/wsgi.py
    WSGIScriptAlias /puppet ${wsgi_dir}/wsgi.py
    ErrorLog logs/puppetboard-error_log
    CustomLog logs/puppetboard-access_log combined
| EOF

    apache::vhost::fragment {'puppetboard-custom':
        vhost    => "${server_name}-ssl",
        priority => 10,
        content  => $puppetboard_custom_fragment,
    }

    apache::vhost::fragment {'puppetboard-static':
        vhost    => "${server_name}-ssl",
        priority => 10,
        content  => epp('ligowebserver/etc/httpd/conf.d/ssldir.epp', {
            description => 'puppetboard static content',
            location    => '/static',
            directory   => "$content_dir/static",
            reqs        => [{
                type         => 'all',
                requirements => [
                    'shib-session',
                    'shib-attr isMemberOf Communities:LSCVirgoLIGOGroupMembers',
                ],
            }],
            use_shib    => $use_shib,
        }),
    }

    apache::vhost::fragment {'puppetboard-wsgi':
        vhost    => "${server_name}-ssl",
        priority => 10,
        content  => epp('ligowebserver/etc/httpd/conf.d/ssldir.epp', {
            description     => 'puppetboard wsgi',
            directory       => $content_dir,
            reqs            => [{
                type         => 'all',
                requirements => [
                    'shib-session',
                    'shib-attr isMemberOf Communities:LSCVirgoLIGOGroupMembers',
                ],
            }],
            custom_fragment => ['WSGIProcessGroup puppetboard', 'WSGIApplicationGroup %{GLOBAL}'],
            use_shib        => $use_shib,
        }),
    }

    apache::vhost::fragment {'puppetboard-dir':
        vhost    => "${server_name}-ssl",
        priority => 10,
        content  => epp('ligowebserver/etc/httpd/conf.d/ssldir.epp', {
            description     => 'puppetboard directory',
            directory       => '/var/www/html/puppetboard',
            reqs            => [{
                type         => 'all',
                requirements => [
                    'shib-session',
                    'shib-attr isMemberOf Communities:LSCVirgoLIGOGroupMembers',
                ],
            }],
            custom_fragment => ['WSGIProcessGroup puppetboard', 'WSGIApplicationGroup %{GLOBAL}'],
            use_shib        => $use_shib,
        }),
    }
}
