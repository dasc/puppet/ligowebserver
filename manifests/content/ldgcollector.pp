# LDG Collector results
#
# @summary Configures access to the ldg collector results
#
# @example Basic usage
#    include ligowebserver::content::ldgcollector
#
class ligowebserver::content::ldgcollector (
    Boolean $use_shib = true,
) {
    if $use_shib {
        include ligowebserver::shib
    } else {
        include ligowebserver::ssl
    }

    $server_name = lookup('apache::server_name', undef, undef, $facts['networking']['fqdn'])

    apache::vhost::fragment {'ldgcollector':
        vhost    => "${server_name}-ssl",
        priority => 10,
        content  => epp('ligowebserver/etc/httpd/conf.d/ssldir.epp', {
            description     => 'LDG collector',
            location        => '/ldg_collector',
            directory       => '/opt/ldg_accounting/collector',
            reqs            => [{
                type         => 'all',
                requirements => [
                    'shib-session',
                    'shib-attr isMemberOf Communities:LSCVirgoLIGOGroupMembers',
                ],
            }],
            custom_fragment => ['Options Indexes'],
            use_shib        => $use_shib,
        }),
    }

    apache::vhost::fragment {'ldgcollector-osg':
        vhost    => "${server_name}-ssl",
        priority => 10,
        content  => epp('ligowebserver/etc/httpd/conf.d/ssldir.epp', {
            description     => 'LDG collector (osg)',
            location        => '/ldg_collector_osg',
            directory       => '/opt/ldg_accounting/collector_osg',
            reqs            => [{
                type         => 'all',
                requirements => [
                    'shib-session',
                    'shib-attr isMemberOf Communities:LSCVirgoLIGOGroupMembers',
                ],
            }],
            custom_fragment => ['Options Indexes'],
            use_shib        => $use_shib,
        }),
    }
}
