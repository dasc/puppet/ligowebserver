# General PHP settings
#
# @summary Configures general php settings to allow php scripts
#
# @example Basic usage
#    include ligowebserver::content::php
#
class ligowebserver::content::php (
    Boolean $use_shib = false,
) {
    if $use_shib {
        include ligowebserver::shib
    } else {
        include ligowebserver::ssl
    }

    $server_name = lookup('apache::server_name', undef, undef, $facts['networking']['fqdn'])

    include ligowebserver::modules::php

    $php_custom_fragment = @(EOF/L)
    ####################
    # apache::vhost::fragment for 'php'
    <FilesMatch \.php$>
        SetHandler application/x-httpd-php
    </FilesMatch>

    #
    # Allow php to handle Multiviews
    #
    AddType text/html .php

    #
    # Add index.php to the list of files that will be served as directory
    # indexes.
    #
    DirectoryIndex index.php

    #
    # Uncomment the following lines to allow PHP to pretty-print .phps
    # files as PHP source code:
    #
    #<FilesMatch \.phps$>
    #    SetHandler application/x-httpd-php-source
    #</FilesMatch>

    #
    # Apache specific PHP configuration options
    # those can be override in each configured vhost
    #
    php_value session.save_handler "files"
    php_value session.save_path    "/var/lib/php/session"


| EOF

    apache::vhost::fragment {'php':
        vhost    => "${server_name}-ssl",
        priority => 10,
        content  => $php_custom_fragment,
    }
}
