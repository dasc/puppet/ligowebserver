# Enable CGI
#
# @summary Configures basic general cgi settings
#
# @example Basic usage
#    include ligowebserver::content::cgi
#
class ligowebserver::content::cgi (
) {
    $server_name = lookup('apache::server_name', undef, undef, $facts['networking']['fqdn'])

    $cgi_custom_fragment = @(EOF/L)

    ####################
    # apache::vhost::fragment for 'cgi'
    AddHandler cgi-script .cgi
    AddType text/html .cgi

    DirectoryIndex index.cgi


| EOF

    apache::vhost::fragment {'cgi':
        vhost    => "${server_name}-ssl",
        priority => 10,
        content  => $cgi_custom_fragment,
    }
}
