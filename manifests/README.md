# ligoprofile::webserver

#### Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with the webserver profiles](#setup)
    * [What these profiles affect](#what-this-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with webserver profiles](#beginning-with-autofs)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
1. [Limitations - OS compatibility, etc.](#limitations)
1. [Development - Guide for contributing to the module](#development)

## Description

This set of profiles is used to configure an apache web server.

These profiles will install and configure the web server, restarting the apache
service as needed.  Convenience profiles are provided to standardize many
of the common settings used on most LDAS webservers.

## Setup

### What these profiles affect

These profiles take complete control over your entire apache configuration.
Any files in /etc/httpd will be replaced with new files created by these
profiles.

These profiles will also ensure that the apache service is running.

If you are setting up a SSL-enabled web server, it is your responsibility to
ensure that the host certificate, key, and cert chain are located in
the correct files.  The CA certificates must be installed in /etc/grid-security/certificates/, which can be done most easily by installing the osg-ca-certs package.

### Setup Requirements

None

### Beginning with webserver profiles

These profiles were written so that in most cases it is only needed to include
the specific content profiles appropriate for your server.

If you would like to add new content profiles, look in the `content/`
subdirectory and model your new profile on the existing ones.

## Usage

Basic Usage: Add content-specific profiles

```
class role::dmt {
    include role::generic
    include profile::webserver::content::autoindex
    include profile::webserver::content::dmt
}
```

Add a content directory using hiera.  First load the shibboleth vhost profile:

```
class role::dmt {
    include role::generic
    include profile::webserver::ssl
}
```

...now use hiera to add new content directories:

```
ligoprofile::apache::ssldirs:
  'archiver':
    'description': 'Disk2Disk status pages'
    'directory': '/var/www/html/archiver'
    'reqs': 
      - type: 'all'
        requirements:
          - 'ip 208.69.128.0/22'
```

Add a directory using hiera, but don't include any access control requirements,
and change the server name (defaults to `$facts['networking']['fqdn']`):

```
apache::server_name: 'apache.ligo-la.caltech.edu'

ligoprofile::apache::ssldirs:
  'archiver':
    'description': 'Disk2Disk status pages'
    'directory': '/var/www/html/archiver'
    'reqs': []
```

Add a directory using hiera, allow directory indexing and executing CGI
scripts, and add a url alias with an alternate index file:

```
ligoprofile::apache::ssldirs:
  'archiver':
    'description': 'Disk2Disk status pages'
    'location': '/archiver'
    'directory': '/opt/archiver'
    'directoryindex': 'index.htm'
    'reqs':
      - type: 'all'
        requirements:
          - 'ip 208.69.128.0/22'
    'custom_fragment':
      - 'Options Indexes ExecCGI'
```

Add a directory using hiera, but require shibboleth.  Note the use of the
alternate hiera variable:

```
ligoprofile::apache::shibdirs:
  'archiver':
    'description': 'Disk2Disk status pages'
    'directory': '/var/www/html/archiver'
    'reqs':
      - type: 'all'
        requirements:
        - 'shib-session'
        - 'shib-attr isMemberOf Communities:LSCVirgoLIGOGroupMembers'
```

Add a directory using hiera with nested access control requirements:

```
ligoprofile::apache::shibdirs:
  'archiver':
    'description': 'Disk2Disk status pages'
    'directory': '/var/www/html/archiver'
    'reqs':
      - type: 'any'
        requirements:
          - type: 'all'
            requirements: 'ip 208.69.128.41'
          - type: 'all'
            requirements: 'ip 208.69.128.37'
          - type: 'all'
            requirements:
              - type: 'all'
                requirements: 'shib-session'
              - type: 'any'
                requirements: 
                  - 'shib-attr isMemberOf Communities:LSCVirgoLIGOGroupMembers'
                  - 'shib-attr isMemberOf gw-astronomy:KAGRA-LIGO:ask.ligo.org'
                  - 'shib-attr isMemberOf gw-astronomy:KAGRA-LIGO:DASWG:wiki'
                  - 'shib-attr eduPersonPrincipalName ~ .*@shibbi.pki.itc.u-tokyo.ac.jp'
```

## Reference

There are two defined types used by these profiles:

* [ligoprofile::httpdirent](https://git.ligo.org/dasc/puppet/ligoprofile/-/blob/master/types/httpdirent.pp): This type is used by the `ligoprofile::webserver::shibdirs` and `ligoprofile::webserver::ssldirs` variables for defining new web server directories.

* [ligoprofile::httprequire](https://git.ligo.org/dasc/puppet/ligoprofile/-/blob/master/types/httprequire.pp): This type is used by the ligoprofile::httpdirent type for defining nested access control requirements.

## Limitations

This is where you list OS compatibility, version compatibility, etc. If there
are Known Issues, you might want to include them under their own heading here.

## Development

Since your module is awesome, other users will want to play with it. Let them
know what the ground rules for contributing are.

## Release Notes/Contributors/Etc. **Optional**

If you aren't using changelog, put your release notes here (though you should
consider using changelog). You can also add any additional sections you feel
are necessary or important to include here. Please use the `## ` header.
