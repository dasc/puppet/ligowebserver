# DMT proxy to the DMT webview application
#
# @summary Configures the DMT proxy to the internal DMT webview application
#
# @example Basic usage
#    include ligowebserver::content::dmt
#
class ligowebserver::content::dmt (
    Boolean $use_shib = true,
) {
    if $use_shib {
        include ligowebserver::shib
    } else {
        include ligowebserver::ssl
    }
    #include ligowebserver::nossl

    $server_name = lookup('apache::server_name', undef, undef, $facts['networking']['fqdn'])

    include ligowebserver::modules::proxy
#    include ligowebserver::modules::rewrite

    $dmt_custom_fragment = @(EOF/L)
    ####################
    # apache::vhost::fragment for 'dmt'
    RewriteEngine on
    RedirectMatch "^/$" "/dmt/Monitors/spiM.html"

    ProxyRequests Off
    ProxyPass /dmtview/LLO http://l1dmt1:9991 disablereuse=On
    ProxyPass /dmtview http://l1dmt1:9991 disablereuse=On

    <Proxy *>
        allow from all
        AuthName "This content is viewable by only LIGO/Virgo personnel. Please enter your LIGO Directory name, e.g. albert.einstein, and password to continue."
        AuthType shibboleth
        ShibRequestSetting requireSession 1
        <RequireAny>
            <RequireAll>
                require shib-session
                require shib-attr isMemberOf Communities:LSCVirgoLIGOGroupMembers
            </RequireAll>
            <RequireAll>
                require shib-session
                require shib-attr isMemberOf Communities:robot:LHOCDSControlRoomSystems
            </RequireAll>
            <RequireAll>
                require shib-session
                require user dmtviewer/cr39.cds.ligo-la.caltech.edu
            </RequireAll>
            <RequireAll>
                require shib-session
                require user host/cr35.cds.ligo-la.caltech.edu
            </RequireAll>
            <RequireAll>
                require shib-session
                require user dmtviewer/cornerdisplay.ligo-la.caltech.edu
            </RequireAll>
            <RequireAny>
                require ip 208.69.128.0/22
                require ip 2607:f390:3ff0::/44
                require local
            </RequireAny>
        </RequireAny>
    </Proxy>


| EOF

    apache::vhost::fragment {'dmt-proxypass-ssl':
        vhost    => "${server_name}-ssl",
        priority => 10,
        content  => $dmt_custom_fragment,
    }
}
