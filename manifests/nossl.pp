# non-ssl vhost
#
# @summary Sets up an apache vhost without SSL.
#
# @note This is normally loaded automatically by a content-specific profile.
#
# @example Basic usage
#    include ligowebserver::nossl
#
# @example Add permissions for a directory
#    include ligowebserver::nossl
#    apache::vhost::fragment {'mydir-http':
#        vhost    => "default-http",
#        priority => 10,
#        content  => epp('ligowebserver/etc/httpd/conf.d/ssldir.epp', {
#            description => 'mydir (http)',
#            directory   => '/var/www/html/mydir',
#            reqs        => [{
#                type         => 'all',
#                requirements => [
#                    'ip 208.69.128.0/22',
#                ],
#            }],
#            use_shib    => false,
#            use_ssl     => false,
#        }),
#    }
#
# @example Add permissions for a directory using hiera
#    It is not currently possible to add new directory accesses to non-ssl
#    webservers using hiera

class ligowebserver::nossl (
) {
    $server_name = lookup('apache::server_name', undef, undef, $facts['networking']['fqdn'])

    include ligowebserver::common

    apache::listen {'80': }

    apache::vhost { "${server_name}-http":
        servername          => $server_name,
        default_vhost       => true,
        port                => 80,
        access_log_file     => 'access_log',
        error_log_file      => 'error_log',
        docroot             => '/var/www/html',
        block               => [ 'scm' ],
        additional_includes => [ ],
        require             => [
            Class['apache::mod::rewrite'],
            Class['apache::mod::headers'],
            Class['apache::mod::cgi'],
        ],
        directories         => [$::ligowebserver::common::apache_default_directory],
    }

    apache::vhost::fragment {'01welcome-http':
        vhost    => "${server_name}-http",
        priority => 10,
        content  => $::ligowebserver::common::apache_welcome_custom_fragment,
    }
    apache::vhost::fragment {'02robots-http':
        vhost    => "${server_name}-http",
        priority => 10,
        content  => $::ligowebserver::common::apache_robots_custom_fragment,
    }

    # Add any non-ssl-accessible directories defined in hiera
    lookup('ligowebserver::apache::nossldirs', undef, undef, {}).each | String $name, Ligowebserver::HttpDirent $dirent | {
        apache::vhost::fragment {$name:
            vhost    => "${server_name}-http",
            priority => 10,
            content  => epp('ligowebserver/etc/httpd/conf.d/ssldir.epp', {
                description     => $dirent['description'],
                location        => $dirent['location'],
                directory       => $dirent['directory'],
                directoryindex  => $dirent['directoryindex'],
                reqs            => $dirent['reqs'],
                custom_fragment => $dirent['custom_fragment'],
                use_shib        => false,
                use_ssl         => false,
            }),
        }
    }
}
