# ilog
#
# @summary Configures access to the ilog web pages
#
# @example Basic usage
#    include ligowebserver::content::ilog
#
class ligowebserver::content::ilog (
    Boolean $use_shib = true,
) {
    if $use_shib {
        include ligowebserver::shib
    } else {
        include ligowebserver::ssl
    }

    include ligowebserver::content::cgi

    $server_name = lookup('apache::server_name', undef, undef, $facts['networking']['fqdn'])

    $ilog_custom_fragment = @(EOF/L)
    ####################
    # apache::vhost::fragment for 'ilog (scripts)'
    ScriptAlias /ilog/pub/ilog.cgi "/export/ilog/ilog/pub/ilog.cgi"
    ScriptAlias /ilog/pub/aux_CGIs "/export/ilog/ilog/pub/aux_CGIs"
    Alias /ilog "/export/ilog/ilog"
| EOF

    apache::vhost::fragment {'ilog-scripts':
        vhost    => "${server_name}-ssl",
        priority => 10,
        content  => $ilog_custom_fragment,
    }

    apache::vhost::fragment {'ilog':
        vhost    => "${server_name}-ssl",
        priority => 10,
        content  => epp('ligowebserver/etc/httpd/conf.d/ssldir.epp', {
            description     => 'ilog (cgi)',
            directory       => '/export/ilog',
            reqs            => [{
                type         => 'all',
                requirements => [
                    'shib-session',
                    'shib-attr isMemberOf Communities:LSCVirgoLIGOGroupMembers',
                ],
            }],
            use_shib        => $use_shib,
            custom_fragment => [
                'Options ExecCGI FollowSymlinks',
                'AllowOverride All',
            ],
        }),
    }
}
