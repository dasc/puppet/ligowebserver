# wsgi module
#
# @summary Loads and enables the wsgi module
#
# @example Basic usage
#    include ligowebserver::modules::wsgi
#
class ligowebserver::modules::wsgi (
) {
    class {'apache::mod::wsgi': }
}
