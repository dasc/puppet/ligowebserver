# Common apache settings for all webservers
#
# @summary Sets up apache with basic settings needed by all webservers.  This normally wouldn't be used directly.  Instead, you should load a specific content profile.
#
# @example Basic usage
#    include ligowebserver::common
#
class ligowebserver::common (
) {
    $server_name = lookup('apache::server_name', undef, undef, $facts['networking']['fqdn'])

    class {'apache':
        servername          => $server_name,
        default_mods        => true,
        default_confd_files => true,
        default_vhost       => false,
        default_ssl_vhost   => false,
        trace_enable        => 'off',
        server_tokens       => 'Minor',
        server_signature    => 'off',
        mpm_module          => 'prefork', # prefork because cgi
    }
    #class {'apache::mod::cgi':} # make ScriptAlias and handlers work (cgid = mpm_event, cgi = mpm_prefork)
    #class {'apache::mod::rewrite':}
    class {'apache::mod::headers':}

    file {'/var/www/html/robots.txt':
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => "User-agent: *\nDisallow: /\n",
    }
    $apache_welcome_custom_fragment = @(EOF/L)

    ####################
    # apache::vhost::fragment for 'welcome page'
    #
    # This configuration enables the default "Welcome" page if there
    # is no default index page present for the root URL.
    <LocationMatch "^/+$">
        Options -Indexes
        ErrorDocument 403 /.noindex.html
    </LocationMatch>

    <Directory /usr/share/httpd/noindex>
        AllowOverride None
        Require all granted
    </Directory>

    Alias /.noindex.html /usr/share/httpd/noindex/index.html


| EOF

    $apache_robots_custom_fragment = @(EOF/L)
    ####################
    # apache::vhost::fragment for 'robots.txt'
    <Directory /var/www/html>
        <Files "robots.txt">
            Require all granted
        </Files>
    </Directory>


| EOF

    $apache_default_directory = lookup('ligowebserver::apache::default_dir')
}
