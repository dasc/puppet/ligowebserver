type Ligowebserver::HttpRequire = Struct[{
    type         => Enum['any', 'all', 'none'],
    requirements => Variant[String, Array[Variant[String,Ligowebserver::HttpRequire]]],
}]
