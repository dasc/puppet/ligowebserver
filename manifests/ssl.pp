# SSL vhost
#
# @summary Sets up an apache SSL vhost.
#
# @note This is normally loaded automatically by a content-specific profile.
#
# @param mozilla_tls_modern
#    Settings to use for TLS.  This gets put into the apache SSLProtocol
#    directive.
#
# @param tls_protos_modern
#    Settings to use for TLS.  This gets put into the apache SSLCipherSuite
#    directive.
#
# @example Basic usage
#    include ligowebserver::ssl
#
# @example Add permissions for a directory
#    include ligowebserver::ssl
#    apache::vhost::fragment {'mydir-ssl':
#        vhost    => "default-ssl",
#        priority => 10,
#        content  => epp('ligowebserver/etc/httpd/conf.d/ssldir.epp', {
#            description => 'mydir (shib)',
#            directory   => '/var/www/html/mydir',
#            reqs        => [{
#                type         => 'all',
#                requirements => [
#                    'ip 208.69.128.0/22',
#                ],
#            }],
#        }),
#    }
#
# @example Add permissions for a directory using hiera
#    include ligowebserver::shib
#
#    ligowebserver::apache::ssldirs:
#      'mydir':
#        'description': 'mydir'
#        'directory': '/var/www/html/mydir'
#        'reqs':
#          - type: 'all'
#            requirements:
#            - 'ip 208.69.128.0/22'
class ligowebserver::ssl (
    Optional[String] $mozilla_tls_modern = undef,
    Optional[String] $tls_protos_modern = undef,
    Optional[String] $docroot = '/var/www/html',
    Optional[Array[String]] $block = ['scm'],
) {
    $server_name = lookup('apache::server_name', undef, undef, $facts['networking']['fqdn'])

    include ligowebserver::common

    apache::listen {'443': }
    class {'apache::mod::ssl':
        ssl_compression => false,
    }

    apache::vhost { "${server_name}-ssl":
        servername          => $server_name,
        default_vhost       => true,
        port                => 443,
        access_log_file     => 'ssl_access_log',
        error_log_file      => 'ssl_error_log',
        ssl                 => true,
        ssl_cipher          => $mozilla_tls_modern,
        ssl_protocol        => $tls_protos_modern,
        ssl_cert            => lookup('ligowebserver::apache::ssl::hostcert'),
        ssl_key             => lookup('ligowebserver::apache::ssl::hostkey'),
        ssl_chain           => lookup('ligowebserver::apache::ssl::hostchain'),
        ssl_certs_dir       => '/etc/grid-security/certificates',
        docroot             => $docroot,
        block               => $block,
        additional_includes => [ ],
        require             => [
            Class['apache::mod::rewrite'],
            Class['apache::mod::headers'],
            Class['apache::mod::cgi'],
        ],
        directories         => [$::ligowebserver::common::apache_default_directory],
    }

    apache::vhost::fragment {'00defaultindex-ssl':
        vhost    => "${server_name}-ssl",
        priority => 10,
        content  => "
   ####################
   # apache::vhost::fragment for 'directory index'
   DirectoryIndex index.html\n",
    }
    apache::vhost::fragment {'10welcome-ssl':
        vhost    => "${server_name}-ssl",
        priority => 10,
        content  => $::ligowebserver::common::apache_welcome_custom_fragment,
    }
    apache::vhost::fragment {'20robots-ssl':
        vhost    => "${server_name}-ssl",
        priority => 10,
        content  => $::ligowebserver::common::apache_robots_custom_fragment,
    }
    apache::vhost::fragment {'01rootdir-ssl':
        vhost    => "${server_name}-ssl",
        priority => 10,
        content  => epp('ligowebserver/etc/httpd/conf.d/ssldir.epp', {
            description => 'Default deny for all filesystem access',
            directory   => '/',
            reqs        => [{
                type         => 'all',
                requirements => 'all denied',
            }],
            use_shib    => false,
        }),
    }

    # Add any ssl-accessible directories defined in hiera
    lookup('ligowebserver::apache::ssldirs', undef, undef, {}).each | String $name, Ligowebserver::HttpDirent $dirent | {
        apache::vhost::fragment {$name:
            vhost    => "${server_name}-ssl",
            priority => 10,
            content  => epp('ligowebserver/etc/httpd/conf.d/ssldir.epp', {
                description     => $dirent['description'],
                location        => $dirent['location'],
                directory       => $dirent['directory'],
                directoryindex  => $dirent['directoryindex'],
                reqs            => $dirent['reqs'],
                custom_fragment => $dirent['custom_fragment'],
                use_shib        => false,
            }),
        }
    }
}
