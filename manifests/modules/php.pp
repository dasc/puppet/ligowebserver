# php module
#
# @summary Loads and enables the php module
#
# @example Basic usage
#    include ligowebserver::modules::php
#
class ligowebserver::modules::php (
) {
    class {'apache::mod::php': }
}
