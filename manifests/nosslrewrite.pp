# non-ssl vhost with redirect
#
# @summary Sets up an apache vhost without SSL that redirects all requests from http to https.
#
# @see ligowebserver::nossl for example usage
#
# @note This is normally loaded automatically by a content-specific profile.
#
# @example Basic usage
#    include ligowebserver::nosslrewrite

class ligowebserver::nosslrewrite (
) {
    $server_name = lookup('apache::server_name', undef, undef, $facts['networking']['fqdn'])
    include ligowebserver::nossl

    apache::vhost { "${server_name}-http":
        servername    => $server_name,
        default_vhost => true,
        port          => 80,
        ssl           => false,
        docroot       => '/var/www/html',
        block         => [ 'scm' ],
        rewrites      => [
            {
                comment      => 'shovel everything over to HTTPS',
                rewrite_cond => ['%{HTTPS} !=on'],
                rewrite_rule => ['^/?(.*) https://%{SERVER_NAME}/$1 [R,L]'],
            },
        ],
        options       => ['-Indexes', '+FollowSymLinks',],
        require       => [
            Class['apache::mod::rewrite'],
            Class['apache::mod::headers'],
        ],
    }
}
